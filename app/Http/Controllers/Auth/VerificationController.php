<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use \Carbon\Carbon;

class VerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function verify(Request $request) {
        if($request->code == auth()->user()->two_factor_code) {
            auth()->user()->registered_at = Carbon::now();
            auth()->user()->save();
            abort(response()->json('User Registered Successfully', 201));
        }
        abort(response()->json('Invalid Verification code', 422));
    }
}
