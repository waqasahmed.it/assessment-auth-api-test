<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function login(Request $request)
    {
        if (Auth::attempt(['user_name' => $request->getUser(), 'password' => $request->getPassword()])) {
            abort(response()->json(['token' => auth()->user()->api_token], 200));
        }
        abort(response()->json('Authentication Failed', 401));
    }
}
