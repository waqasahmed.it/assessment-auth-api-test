<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'unique:users', 'min:4', 'max:20'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'is_admin' => ['required']
        ]);
    }
    
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'user_role' => ($data['is_admin'] == 1)? 'admin': 'user',
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60),
        ]);
        $user->generateTwoFactorCode();
        return $user;
    }
    
    
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            abort(response()->json($validator->errors()->first(), 409));
        }
        $user = $this->create($request->all()); 
        abort(response()->json(['access_token' => $user->api_token, 'message' => 'Copy your access_token and Check Your Email.'], 201));
    }
}
