<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\SendInvitation;
use Illuminate\Support\Facades\Mail;
use Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function inviteUser(Request $request) 
    {
        if(auth()->user()->user_role == 'admin') {
            Notification::route('mail', $request->email)->notify(new SendInvitation());
            abort(response()->json('Invitiation for User Registration sent successfully!', 200));
        }
        abort(response()->json('Invalid User Permission', 403));
    }
    
    public function updateProfile(Request $request)
    {
        if(isset($request->name)) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255']
            ]);
            if($validator->fails())
                abort(response()->json($validator->errors()->first(), 409));
            else
                auth()->user()->name = $request->name;
        }
        
        if(isset($request->user_name)) {
            $validator = Validator::make($request->all(), [
                'user_name' => ['required', 'string', 'unique:users', 'min:4', 'max:20'],
            ]);
            if($validator->fails())
                abort(response()->json($validator->errors()->first(), 409));
            else
                auth()->user()->user_name = $request->user_name;
        }
        
        if(isset($request->password)) {
            $validator = Validator::make($request->all(), [
                'password' => ['required', 'string', 'min:8'],
            ]);
            if($validator->fails())
                abort(response()->json($validator->errors()->first(), 409));
            else
                auth()->user()->password = Hash::make($request->password);
        }
        
        if(isset($request->is_admin)) {
            auth()->user()->user_role = ($request->is_admin == 1)? 'admin': 'user';
        }
        
        if($request->hasFile('avatar')) {
            if(auth()->user()->avatar != null) {
                if (File::exists(auth()->user()->avatar)) {
                    File::delete(auth()->user()->avatar);
                }
            }
            $file = $request->file('avatar');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move(public_path('/uploads/avatar/'),$filename);
            
            auth()->user()->avatar = 'uploads/avatar/'.$filename;
        }
        
        auth()->user()->save();
        abort(response()->json('User Profile Updated', 200));
    }
}
