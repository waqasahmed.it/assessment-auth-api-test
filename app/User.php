<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\TwoFactorCode;

class User extends Authenticatable
{
    use Notifiable;


    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'registered_at',
        'two_factor_expires_at',
    ];

    protected $fillable = [
        'name',
        'user_name',
        'email',
        'avatar',
        'user_role',
        'registered_at',
        'password',
        'api_token',
        'two_factor_code',
        'two_factor_expires_at',
        'created_at',
        'updated_at',
        'deleted_at',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = rand(100000, 999999);
        $this->save();
        $this->notify(new TwoFactorCode());
    }

}
