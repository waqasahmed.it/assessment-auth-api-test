<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::post('/register', 'Auth\RegisterController@register')->name('userRegistration');

Route::middleware('auth:api')->post('/verify', 'Auth\VerificationController@verify')->name('userVerification');
Route::middleware('auth:api')->post('/invite', 'HomeController@inviteUser')->name('inviteUserForRegistration');
Route::middleware('auth:api')->post('/profile/update', 'HomeController@updateProfile')->name('updateUserProfile');